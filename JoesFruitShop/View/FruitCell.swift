//
//  FruitCell.swift
//  Joe'sFruitShop
//
//  Created by Mac on 17/02/2022.
//

import UIKit

class FruitCell: UITableViewCell {
    
    var index: IndexPath?
    //pull the data frome the main view controller
    var data: fruitStruct!{
        didSet {
            updateUi()
        }
    }
    //get the biulding information to each army
    func updateUi(){
        fruitName = data.name ?? ""
        fruitImage = data.image!
    }
    var tableViewWidth = CGFloat()
    var fruitName = String()
    var fruitImage = String()
    let backImage = UIImageView()
    var titleTextField = UITextField()
    var fruitWebImage = UIImageView()
    lazy var backView: UIView = {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableViewWidth, height: 180))
        view.layer.borderWidth = 6.0
        view.layer.borderColor = UIColor.lightGray.cgColor
        view.backgroundColor = .black
        view.layer.cornerRadius = 18
        return view
    }()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func layoutSubviews() {
        addSubview(backView)
        sendSubviewToBack(backView)
        contentView.backgroundColor = UIColor.clear
        backgroundColor = UIColor.clear
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        //set ithe image of the fruit
        setImageAndText(fruitName:fruitName, imageName: fruitImage)
    }
    func setImageAndText(fruitName:String,imageName:String){
        //set the imag from the server
        let imageUrl:NSURL? = NSURL(string: imageName)
        if let url = imageUrl {
            let placeHolde = UIImage(named: "back")
            fruitWebImage.sd_setImage(with: url as URL, placeholderImage: placeHolde)
        }
        fruitWebImage.frame = CGRect(x: 6, y: 6, width: tableViewWidth/2.2, height: 168)
        fruitWebImage.contentMode = .scaleToFill
        fruitWebImage.layer.cornerRadius = 10
        addSubview(fruitWebImage)
        fruitWebImage.clipsToBounds = true
        titleTextField.textAlignment = NSTextAlignment.center
        titleTextField.textColor = .white
        titleTextField.backgroundColor = .clear
        titleTextField.font = .boldSystemFont(ofSize: 31)
        titleTextField.isUserInteractionEnabled = false
        titleTextField.sizeToFit()
        titleTextField.frame = CGRect(x: tableViewWidth/2.2, y: 70, width: tableViewWidth/1.8, height: 40)
        titleTextField.text = "\(fruitName)"
        addSubview(titleTextField)
        bringSubviewToFront(titleTextField)
    }
}
