//
//  Fruit.swift
//  Joe'sFruitShop
//
//  Created by Mac on 17/02/2022.
//

import Foundation
import UIKit

struct food: Decodable{
    var fruits:[fruitStruct]
}
//a struct of potential jason from the server
struct fruitStruct: Decodable{
    var name: String?
    var image: String?
    var description: String?
    var price: Int?
}
//a class of fruit list's
class Fruits{
    static func fetchFruit() ->[fruitStruct]{
        let fr1 = fruitStruct(name: "avocado", image: "avocado Image", description: "avocado is good", price: 7)
        let fr2 = fruitStruct(name: "avocado", image: "avocado Image", description: "avocado is good", price: 7)
        let fr3 = fruitStruct(name: "avocado", image: "avocado Image", description: "avocado is good", price: 7)
        return [fr1,fr2,fr3]
    }
}
